package com.fp.demos.spring.integration.core.web;

import com.fp.demos.spring.integration.core.domain.Asset;
import com.fp.demos.spring.integration.core.service.AssetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/assets")
public class AssetController {

    @Autowired
    private AssetService assetService;

    @RequestMapping("/")
    public List<Asset> getAssets() {
        return assetService.getAssets();
    }

    @RequestMapping("/{id}")
    public Asset getAsset(@PathVariable("id") String id) {
        return assetService.getAsset(id);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String createAsset(@RequestBody Asset asset) {
        assetService.createAsset(asset);
        return "OK";
    }
}
