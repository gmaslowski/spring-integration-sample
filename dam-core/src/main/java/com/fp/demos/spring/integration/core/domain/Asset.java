package com.fp.demos.spring.integration.core.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@ToString
public class Asset implements Serializable {
    public enum AssetState {
        NEW, READY
    }

    public enum AssetType {
        TRANSCODABLE, NON_TRANSCODABLE
    }

    public final String id;
    public final String content;
    public final String originatingSite;
    public final List<String> desiredSites;
    public final AssetType assetType;
    public final AssetState assetState;

    @JsonCreator
    public Asset(
            @JsonProperty("id") String id,
            @JsonProperty("content") String content,
            @JsonProperty("originatingSite") String originatingSite,
            @JsonProperty("desiredSites") List<String> desiredSites,
            @JsonProperty("assetType") AssetType assetType,
            @JsonProperty("assetState") AssetState assetState) {
        this.id = id;
        this.content = content;
        this.originatingSite = originatingSite;
        this.desiredSites = desiredSites;
        this.assetType = assetType;
        this.assetState = assetState;
    }
}
