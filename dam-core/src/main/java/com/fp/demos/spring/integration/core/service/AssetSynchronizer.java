package com.fp.demos.spring.integration.core.service;

import com.fp.demos.spring.integration.core.domain.Asset;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;

import javax.jms.*;
import java.util.List;

@MessageEndpoint
public class AssetSynchronizer {

    private static final Logger LOGGER = Logger.getLogger(AssetSynchronizer.class);

    @Autowired
    private ConnectionFactory connectionFactory;

    @Value("node.name")
    private String nodeName;

    @ServiceActivator(inputChannel = "readyAssets")
    public void synchronize(Message<Asset> assetMessage) throws JMSException {
        Connection connection = connectionFactory.createConnection();
        connection.start();

        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        assetMessage.getHeaders().get("desiredSites", List.class)
                .stream()
                .filter(site -> !site.equals(nodeName) && !site.equals(assetMessage.getPayload().originatingSite))
                .forEach(site -> sendToSite(assetMessage, session, (String) site));

        session.close();
        connection.close();
    }

    private void sendToSite(Message<Asset> assetMessage, Session session, String site) {
        try {
            MessageProducer producer = session.createProducer(session.createQueue(site));
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

            producer.send(session.createObjectMessage(assetMessage.getPayload()));
            LOGGER.info(String.format("Sent an asset [%s] to site [%s]", assetMessage.getPayload(), site));
            producer.close();
        } catch (JMSException je) {
            LOGGER.error("Problem while sending JMS message.", je);
        }
    }
}
