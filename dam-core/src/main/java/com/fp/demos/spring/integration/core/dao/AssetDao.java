package com.fp.demos.spring.integration.core.dao;

import com.fp.demos.spring.integration.core.domain.Asset;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class AssetDao {

    private Map<String, Asset> assets = new HashMap<>();

    public Asset getById(String id) {
        return assets.get(id);
    }

    public void persist(Asset asset) {
        assets.put(asset.id, asset);
    }

    public List<Asset> getAssets() {
        return Lists.newArrayList(assets.values());
    }
}
