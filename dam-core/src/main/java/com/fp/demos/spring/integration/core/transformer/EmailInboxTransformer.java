package com.fp.demos.spring.integration.core.transformer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fp.demos.spring.integration.core.domain.Asset;
import com.fp.demos.spring.integration.core.messaging.AssetMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.Transformer;
import org.springframework.messaging.Message;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;

public class EmailInboxTransformer {

    @Autowired
    private ObjectMapper objectMapper;

    @Transformer
    public Message<Asset> inboxEmailToInboundAsset(MimeMessage email) {
        try {
            return new AssetMessage(objectMapper.readValue(email.getInputStream(), Asset.class));
        } catch (IOException | MessagingException e) {
            return null;
        }
    }
}
