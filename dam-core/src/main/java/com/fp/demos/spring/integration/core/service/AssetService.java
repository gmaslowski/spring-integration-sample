package com.fp.demos.spring.integration.core.service;

import com.fp.demos.spring.integration.core.dao.AssetDao;
import com.fp.demos.spring.integration.core.domain.Asset;
import com.fp.demos.spring.integration.core.domain.AssetMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AssetService {

    @Autowired
    @Qualifier("inboundAssets")
    private MessageChannel inboundAssets;

    @Autowired
    private AssetDao assetDao;

    public void createAsset(final Asset asset) {
        AssetMessage message = new AssetMessage(asset);
        inboundAssets.send(message);
    }

    public Asset getAsset(String id) {
        return assetDao.getById(id);
    }

    public List<Asset> getAssets() {
        return assetDao.getAssets();
    }
}
