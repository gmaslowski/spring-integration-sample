package com.fp.demos.spring.integration.core.messaging;

import com.fp.demos.spring.integration.core.domain.Asset;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;

import java.util.HashMap;
import java.util.Map;

public class AssetMessage implements Message<Asset> {

    private final Asset asset;
    private final MessageHeaders messageHeaders;

    public AssetMessage(Asset asset) {
        this.asset = asset;
        Map<String, Object> headers = new HashMap<>();
        headers.put("type", asset.assetType.name());
        headers.put("desiredSites", asset.desiredSites);
        this.messageHeaders = new MessageHeaders(headers);
    }

    @Override
    public Asset getPayload() {
        return asset;
    }

    @Override
    public MessageHeaders getHeaders() {
        return messageHeaders;
    }
}
