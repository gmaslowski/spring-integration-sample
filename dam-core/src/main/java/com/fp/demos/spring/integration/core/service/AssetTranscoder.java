package com.fp.demos.spring.integration.core.service;

import com.fp.demos.spring.integration.core.domain.Asset;
import org.apache.log4j.Logger;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;

@MessageEndpoint
public class AssetTranscoder {

    private static final Logger LOGGER = Logger.getLogger(AssetTranscoder.class);

    @ServiceActivator(inputChannel = "assetsToTranscode", outputChannel = "readyAssets")
    public Message<Asset> transcodeAsset(Message<Asset> asset) throws InterruptedException {
        LOGGER.info(String.format("Starting transcoding of asset: [%s].", asset.getPayload()));
        // Do nothing special, just wait a bit.
        Thread.sleep(2000);
        LOGGER.info(String.format("Finished transcoding of asset: [%s].", asset.getPayload()));
        return asset;
    }
}
