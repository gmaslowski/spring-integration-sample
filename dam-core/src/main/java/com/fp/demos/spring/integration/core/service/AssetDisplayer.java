package com.fp.demos.spring.integration.core.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fp.demos.spring.integration.core.domain.Asset;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;

@MessageEndpoint
public class AssetDisplayer {

    private static final Logger LOGGER = Logger.getLogger(AssetDisplayer.class);

    @Autowired
    private ObjectMapper objectMapper;

    @ServiceActivator(inputChannel = "readyAssets")
    public void processMessage(Message<Asset> assetMessage) throws JsonProcessingException {
        LOGGER.info(String.format("Received asset [%s].", assetMessage.getPayload()));
    }
}
