package com.fp.demos.spring.integration.core.service;

import com.fp.demos.spring.integration.core.dao.AssetDao;
import com.fp.demos.spring.integration.core.domain.Asset;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;

import static com.fp.demos.spring.integration.core.domain.Asset.AssetState.NEW;
import static com.fp.demos.spring.integration.core.domain.Asset.AssetState.READY;
import static com.google.common.base.Objects.firstNonNull;

@MessageEndpoint
public class AssetPersister {

    private static final Logger LOGGER = Logger.getLogger(AssetPersister.class);

    @Value("${node.name}")
    private String nodeName;

    @Autowired
    private AssetDao assetDao;

    @ServiceActivator(inputChannel = "inboundAssets")
    public void persistAssetInitially(Asset asset) {
        doPersistAsset(new Asset(
                asset.id,
                asset.content,
                firstNonNull(asset.originatingSite, nodeName),
                asset.desiredSites,
                asset.assetType,
                NEW));
    }


    @ServiceActivator(inputChannel = "readyAssets")
    public void persistAssetWhenReady(Asset asset) {
        doPersistAsset(new Asset(
                asset.id,
                asset.content,
                asset.originatingSite,
                asset.desiredSites,
                asset.assetType,
                READY));
    }

    private void doPersistAsset(Asset asset) {
        LOGGER.info(String.format("Persisting asset [%s].", asset));
        assetDao.persist(asset);
    }

}
